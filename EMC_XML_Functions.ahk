
;xmlDoc.setProperty("SelectionNamespaces", "xmlns:mouic='clr-namespace:Micros.OpsUI.Controls;assembly=OpsUI' xmlns='clr-namespace:EMC.Application.General.Controls.MicrosGridEditor;assembly=EMC'")




extractButton(xmlDoc, index=0)
{
	global BUTTON_XPATH
	allButtons := xmlDoc.selectNodes(BUTTON_XPATH)
	if index < 0
		return allButtons
	else
		return allButtons.item(index)
}

setMenuID(ByRef b, menuID)
{
	x := ComObjCreate("MSXML2.DOMDocument.6.0")
	nodeCommandNumber := x.createAttribute("OpsCommandNumber")
	nodeCommandNumber.value := menuID
	b.setAttributeNode(nodeCommandNumber)
}

getMenuID(b)
{
	return b.getAttribute("OpsCommandNumber")
}

;Extracts Node containing button text from @xmlDoc
;@index - index of item to retrieve (send negative index to retrieve entire array)
extractButtonText(xmlDoc, index=0)
{
	global TEXT_XPATH
	allTexts := xmlDoc.selectNodes(TEXT_XPATH)
	if index < 0
		return allTexts
	else
		return allTexts.item(index)
}

getButtonText(b)
{
	return b.getAttribute("Text")
}

setButtonText(ByRef b, txt)
{
	x := ComObjCreate("MSXML2.DOMDocument.6.0")
	nodeText := x.createAttribute("Text")
	nodeText.value := txt
	b.setAttributeNode(nodeText)
}

setGenericAttribute(ByRef b, attribute, value)
{
	x := ComObjCreate("MSXML2.DOMDocument.6.0")
	nodeAttrib := x.createAttribute(attribute)
	nodeAttrib.value := value
	b.setAttributeNode(nodeAttrib)
}

loadXML(ByRef data)
{
  o := ComObjCreate("MSXML2.DOMDocument.6.0")
  o.async := false
  o.loadXML(data)
  return o
}



