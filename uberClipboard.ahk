; AutoHotkey Version: 1.x
; Language:       English
; Platform:       Win9x/NT
; Author:         James UK 07/11/08 - Freeware
;
; Change since 30/10/08 version; Changed "Send" to "SendInput" to improve speed. Commented out icon line.
;
; Script Function: Read each line of a chosen text file one by one, and insert it to your
; active window or field when the F12 key is pressed.
;
; Provides an on-screen ToolTip for 5 seconds to show user the contents of the next line to be inserted, and also the same
; details when the mouse is hovered over the script's icon in the System Tray.
;
; Don't forget, if you have a few special symbols in your text files, like "!", you'll need to encapsulate them in
; "curly brackets" thus: {!}. That way, the script can keep the functionality of other keystroke commands like {Enter} etc.
;
; Template script (you can customize this template by editing "ShellNew\Template.ahk" in your Windows folder)
;
EMC_BARCODES_LIST = WindowsForms10.Window.8.app.0.378734a3
EMC_BARCODES_MASTER_EDIT = WindowsForms10.RichEdit20W.app.0.378734a2

Start:
	#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
	#NoTrayIcon
	#SingleInstance Force
	SendMode Input  
	SetWorkingDir %A_ScriptDir%  
	Menu, Tray, Tip, Flickr Field Inserter ; Shows text on mouse hover over icon in System Tray, just whilst file select dialogue box is open.
	;Menu, Tray, Icon, flickr.ico ; My choice of System Tray icon to use. Substitute as appropriate.
	
	lineno = 0
	linecount = 0
	lineArray := Object()
	lineArray.insert("")
	
	if 0 < 1
	{
		FileSelectFile, filename, 1,,Please choose a text file to read from;, *.txt
		If ErrorLevel = 1
		{   Msgbox, 5,No file to work with..?, You must choose a file to read!
		   IfMsgBox Retry
				  Goto Start
		   else
				  MsgBox,,Bye!,OK, see you soon!
		   ExitApp
		}
		
		Loop, Read, %filename%
		{
			if (A_LoopReadLine = "")
				continue
			else {
				lineArray.insert(A_LoopReadLine)
				linecount++
			}
		}
	} else {
		contents = %1%
		if (contents = "c") {
			contents := Clipboard
		}
		;Uncomment the following line for easy debugging
		;TrayTip, UberClip,% "Uber contents: " . contents
		Loop, Parse, contents,`r, `n
		{
			if (A_LoopField = "") or not RegExMatch(A_LoopField, "\S")
				continue
			else {
				lineArray.insert(A_LoopField)
				linecount++
			}
		}
	}
	Clipboard=
	GoSub,AdvanceList
return

#IFWinActive

;Paste current entry and advance list
^Insert::
uberPaste:
	Keywait, Control
	Gosub,AdvanceList
	if (line != "")
	{
		SendLevel 1
		ControlGetFocus, c, A
		if (c = EMC_BARCODES_LIST or c = EMC_BARCODES_MASTER_EDIT) {
			Send %Clipboard%
		} else {
			Send ^v
		}
		Sleep 300
	}
return

^+Insert::
	Keywait, Shift
	Gosub,uberPaste
	if (line != "")
		Send {Enter}
return

^PgDn::
AdvanceList:
	if (lineno <= linecount) {
		lineno++
		Gosub,PastePrep
	} else {
		Gosub,Exiter
	}
return

^PgUp::
RegressList:
	if (lineno > 1)
		lineno--
	Gosub,PastePrep
return

^#Enter::
PastePrep:
	line := lineArray[lineno]
	nextLine := lineArray[lineno + 1]
	Gosub,ShowToolTip
	if (lineno == 1)
		Clipboard := nextline
	else
		Clipboard := line
	Clipwait, 1
return

Exiter:
	MsgBox, You've reached the end of the list.
ExitApp

ShowToolTip:
	line_index := 0
	if (lineno <= 1)
		line_index := "-"
	else
		line_index := lineno - 1
	if (nextLine != "") {
		ToolTip, Current line: "%line%"`nNext line: "%nextLine%"`n%line_index% of %linecount%
	} else {
		ToolTip, END
	}
	SetTimer, RemoveToolTip, -5000
return

RemoveToolTip:
	ToolTip
return

; def convert_id_notation(item_id):
	; if 'M' in item_id:
		; split_item_id = item_id.split('M')
		; zero_pad = 9 - (len(split_item_id[0]) + len(split_item_id[1]))
		; return '{0}{1}{2}'.format(split_item_id[0], '0'*zero_pad, split_item_id[1])