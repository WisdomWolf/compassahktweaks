; Online Ref.:  http://www.autohotkey.com/forum/viewtopic.php?t=63673
;
; Last Update:  15/Mar/2010 17:30
;
; Created by:   MasterFocus
;               http://www.autohotkey.net/~MasterFocus/AHK/
;
; Thanks to:    Lexikos, for improving it significantly
;               http://www.autohotkey.com/forum/topic43826.html#267338
;
;========================================================================
;
; This template contains two examples by default. You may remove them.
;
; * HOW TO ADD A PROGRAM to be checked upon (de)activation/(un)existance:
;
; 1. Add a variable named ProgWinTitle# (Configuration Section)
; containing the desired title/ahk_class/ahk_id/ahk_group
;
; 2. Add a variable named WinTrigger# (Configuration Section)
; containing the desired trigger ("Exist" or "Active")
;
; 3. Add labels named LabelTriggerOn# and/or LabelTriggerOff#
; (Custom Labels Section) containing the desired actions
;
; 4. You may also change CheckPeriod value if desired
;
;========================================================================

#Persistent

; ------ ------ CONFIGURATION SECTION ------ ------

SetActiveWindow(window, number, trigger="Active")
{
	global ProgWinTitle1
	ProgWinTitle%number% := window
	global WinTrigger1
	WinTrigger1 := trigger
	TrayTip, CompassAHKTweaks, % "Active window is " . ProgWinTitle1 . "`nTrigger: " . WinTrigger1
}

; SetTimer Period
CheckPeriod = 200

; ------ END OF CONFIGURATION SECTION ------ ------

SetTimer, LabelCheckTrigger, %CheckPeriod%
Return

; ------ ------ ------

LabelCheckTrigger:
  While ( ProgWinTitle%A_Index% != "" && WinTrigger := WinTrigger%A_Index% )
    if ( !ProgRunning%A_Index% != !Win%WinTrigger%( ProgWinTitle := ProgWinTitle%A_Index% ) )
      GoSubSafe( "LabelTriggerO" ( (ProgRunning%A_Index% := !ProgRunning%A_Index%) ? "n" : "ff" ) A_Index )
Return

; ------ ------ ------

GoSubSafe(mySub)
{
  if IsLabel(mySub)
    GoSub %mySub%
}

; ------ ------ CUSTOM LABEL SECTION ------ ------

LabelTriggerOn1:
	
return
LabelTriggerOff1:
	TrayTip, CompassAHKTweaks, % "Focus lost. Attempting to regain"
	WinActivate, % ProgWinTitle1
return
LabelTriggerOn2:
Return
LabelTriggerOff2:
return

; ------ END OF CUSTOM LABEL SECTION ------ ------