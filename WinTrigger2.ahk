; Original script by MasterFocus. See original post for instructions.
; LINK: http://www.autohotkey.com/board/topic/59059-wintrigger-watch-deactivationunexistance-of-windows/

#Persistent

; ------ ------ CONFIGURATION SECTION ------ ------

; Program Titles
ProgWinTitle1 = ahk_class Notepad
ProgWinTitle2 = Calculator

; SetTimer Period
CheckPeriod = 750

; ------ END OF CONFIGURATION SECTION ------ ------

SetTimer, LabelCheckLoadClose, %CheckPeriod%
Return

; ------ ------ ------

LabelCheckLoadClose:
Loop
{

	if ProgWinTitle%A_Index% =
		break

	if (!ProgRunning%A_Index% != !WinExist(ProgWinTitle%A_Index%))
	{

		if ProgRunning%A_Index% := !ProgRunning%A_Index%
			GoSub #1
		else
			GoSub #2

	}
}
return

; Simple helper function for brevity:
GoSubSafe(Sub) {

if IsLabel(Sub)
	GoSub %Sub%

}

; ------ ------ CUSTOM LABEL SECTION ------ ------

LabelOnOpen:
	Send #1
LabelOnClose:
	Send #2
LabelOnOpen2:
	MsgBox % A_ThisLabel
return

; ------ END OF CUSTOM LABEL SECTION ------ ------


#1::
	Gui, Color, EEAA99
	Gui +LastFound ;9 Make the GUI window the last found window for use by the line below.
	WinSet, TransColor, EEAA99To

	Gui -Caption

	Gui, Add, Button, Default, Test
	Gui, Show, x1200 y100
return



ButtonTest:
	Winactivate, New Text Document - Notepad
	Send 123456



#2::
	Gui, Destroy